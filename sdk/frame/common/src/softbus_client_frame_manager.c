/*
 * Copyright (c) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "softbus_client_frame_manager.h"

#include <securec.h>

#include <string.h>
#include "client_bus_center_manager.h"
#include "client_disc_manager.h"
#include "client_trans_session_manager.h"
#include "softbus_client_event_manager.h"
#include "softbus_client_stub_interface.h"
#include "softbus_def.h"
#include "softbus_errcode.h"
#include "softbus_feature_config.h"
#include "softbus_log.h"

// 软总线初始化标志，防止重复进行初始化。
static bool g_isInited = false;
// 静态的初始化互斥锁。
static pthread_mutex_t g_isInitedLock = PTHREAD_MUTEX_INITIALIZER;
// 存放传入的pkgName。
static char g_pkgName[PKG_NAME_SIZE_MAX] = {0};

// 注销操作，进行一些必要的释放和清理。
static void ClientModuleDeinit(void)
{
    EventClientDeinit();
    BusCenterClientDeinit();
    TransClientDeinit();
    DiscClientDeinit();
}

static int32_t ClientModuleInit()
{
    //设置一堆默认的参数，包括g_tranConfig等，暂时没有具体看参数内容
    SoftbusConfigInit();

    //初始化g_observerList,一种SoftBusList
    if (EventClientInit() == SOFTBUS_ERR) {
        SoftBusLog(SOFTBUS_LOG_COMM, SOFTBUS_LOG_ERROR, "init event manager failed");
        //如果初始化失败则跳转至错误处理的代码段，下同。
        goto ERR_EXIT;
    }

    //初始化g_busCenterClient中的各个SoftBusList,初始化BusCenterServerProxy
    if (BusCenterClientInit() == SOFTBUS_ERR) {
        SoftBusLog(SOFTBUS_LOG_COMM, SOFTBUS_LOG_ERROR, "init bus center failed");
        goto ERR_EXIT;
    }

    // g_discInfo初始化，DiscServerProxy初始化
    if (DiscClientInit() == SOFTBUS_ERR) {
        SoftBusLog(SOFTBUS_LOG_COMM, SOFTBUS_LOG_ERROR, "init service manager failed");
        goto ERR_EXIT;
    }

    // g_idFlagBitmap、g_clientSessionServerList、TransServerProxy初始化，注册回调函数
    if (TransClientInit() == SOFTBUS_ERR) {
        SoftBusLog(SOFTBUS_LOG_COMM, SOFTBUS_LOG_ERROR, "init connect manager failed");
        goto ERR_EXIT;
    }

    return SOFTBUS_OK;

// 进行错误处理，主要是错误日记的打印以及注销操作。
ERR_EXIT:
    SoftBusLog(SOFTBUS_LOG_COMM, SOFTBUS_LOG_ERROR, "softbus sdk frame init failed.");
    ClientModuleDeinit();
    return SOFTBUS_ERR;
}

int32_t InitSoftBus(const char *pkgName)
{
    // pkgName 合法性检查。
    if (pkgName == NULL || strlen(pkgName) >= PKG_NAME_SIZE_MAX) {
        SoftBusLog(SOFTBUS_LOG_COMM, SOFTBUS_LOG_ERROR, "init softbus sdk fail.");
        return SOFTBUS_ERR;
    }

    // 若软总线已经完成初始化，则退出，否则继续进行初始化。
    if (g_isInited == true) {
        return SOFTBUS_OK;
    }

    // 加锁
    if (pthread_mutex_lock(&g_isInitedLock) != 0) {
        SoftBusLog(SOFTBUS_LOG_COMM, SOFTBUS_LOG_ERROR, "lock failed");
        return SOFTBUS_LOCK_ERR;
    }

    // 若软总线已经完成初始化，解锁且退出初始化。
    if (g_isInited == true) {
        pthread_mutex_unlock(&g_isInitedLock);
        return SOFTBUS_OK;
    }

    // 将pkgName复制给g_pkgName。
    if (strcpy_s(g_pkgName, sizeof(g_pkgName), pkgName) != EOK) {
        // 若复制失败，首先需要解锁，否则临界区的资源将无法被其他线程访问。下同。
        pthread_mutex_unlock(&g_isInitedLock);
        SoftBusLog(SOFTBUS_LOG_COMM, SOFTBUS_LOG_ERROR, "strcpy_s failed.");
        return SOFTBUS_MEM_ERR;
    }

    // 对部分变量和回调函数进行初始化
    if (ClientModuleInit() != SOFTBUS_OK) {
        SoftBusLog(SOFTBUS_LOG_COMM, SOFTBUS_LOG_ERROR, "ctx init fail");
        pthread_mutex_unlock(&g_isInitedLock);
        return SOFTBUS_ERR;
    }

    // 软总线 IPC Client端服务提供方初始化。
    if (ClientStubInit() != SOFTBUS_OK) {
        SoftBusLog(SOFTBUS_LOG_COMM, SOFTBUS_LOG_ERROR, "service init fail");
        pthread_mutex_unlock(&g_isInitedLock);
        return SOFTBUS_ERR;
    }

    //初始化成功后进行标记，并解锁。
    g_isInited = true;
    pthread_mutex_unlock(&g_isInitedLock);
    SoftBusLog(SOFTBUS_LOG_COMM, SOFTBUS_LOG_INFO, "softbus sdk frame init success.");
    return SOFTBUS_OK;
}

//把 g_pkgName赋给 name
int32_t GetSoftBusClientName(char *name, uint32_t len)
{
    if (name == NULL || len < PKG_NAME_SIZE_MAX) {
        SoftBusLog(SOFTBUS_LOG_COMM, SOFTBUS_LOG_ERROR, "invalid param");
        return SOFTBUS_ERR;
    }

    if (strncpy_s(name, len, g_pkgName, strlen(g_pkgName)) != EOK) {
        SoftBusLog(SOFTBUS_LOG_COMM, SOFTBUS_LOG_ERROR, "strcpy fail");
        return SOFTBUS_ERR;
    }

    return SOFTBUS_OK;
}

int32_t CheckPackageName(const char *pkgName)
{
    char clientPkgName[PKG_NAME_SIZE_MAX] = {0};
    //把 g_pkgName赋给了clientPkgName
    if (GetSoftBusClientName(clientPkgName, PKG_NAME_SIZE_MAX) != SOFTBUS_OK) {
        SoftBusLog(SOFTBUS_LOG_COMM, SOFTBUS_LOG_ERROR, "GetSoftBusClientName err");
        return SOFTBUS_INVALID_PKGNAME;
    }
    //然后检查clientPkgName和传入的pkgName是否一致
    if (strcmp(clientPkgName, pkgName) != 0) {
        return SOFTBUS_INVALID_PKGNAME;
    }
    return SOFTBUS_OK;
}

